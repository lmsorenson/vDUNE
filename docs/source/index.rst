.. vDUNE documentation master file, created by
   sphinx-quickstart on Fri Aug 13 19:30:04 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

vDUNE
=================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:
   
   vision
   usecases
